import React from "react"
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Linking,
  Alert,
  Share,
  ImageStyle
} from "react-native"
import { NavigationStackScreenOptions, NavigationScreenProp } from "react-navigation"

import metrics from "../config/metrics"
import CustomButton from "../components/CustomButton"
import EventController, { Event } from "../controller/EventController"
import { User } from "../controller/AuthController"

const DEFAULT_AVA = require("../../assets/ava_example.png")
const ICON_PHONE = require("../../assets/ic_phone.png")
const ICON_GENDER = require("../../assets/ic_gender_both.png")
const ICON_TIME = require("../../assets/ic_time.png")
const ICON_MONEY = require("../../assets/ic_money.png")
const ICON_LOCATION = require("../../assets/ic_location.png")
const ICON_THUMBS = require("../../assets/ic_thumb.png")
const ICON_SHARE = require("../../assets/ic_share.png")

const CATEGORY_JOGGING = require("../../assets/sport_category_jogging.png")
const CATEGORY_FOOTBALL = require("../../assets/sport_category_football.png")
const CATEGORY_BADMINTON = require("../../assets/sport_category_badminton.png")
const CATEGORY_SWIM = require("../../assets/sport_category_swim.png")
const CATEGORY_BASKET = require("../../assets/sport_category_basket.png")

interface State {
  event: Event
  merge: boolean
}

interface Props {
  navigation: NavigationScreenProp<any, any>
}

export default class EventDetail extends React.Component<Props, State> {
  static navigationOptions: NavigationStackScreenOptions = {
    title: "Event Detail"
  }

  state = {
    event: this.props.navigation.getParam("event"),
    merge: this.props.navigation.getParam("merge")
  }

  constructor(props: Props) {
    super(props)
    this.handleLocationPressed = this.handleLocationPressed.bind(this)
    this.handleJoinPressed = this.handleJoinPressed.bind(this)
    this.handleWhoHasJoinedPressed = this.handleWhoHasJoinedPressed.bind(this)
    this.handleShareButtonPressed = this.handleShareButtonPressed.bind(this)
  }

  // Get the corresponding category image based on event type
  getCategoryImage() {
    switch (this.state.event.type) {
      case "Jogging":
        return CATEGORY_JOGGING
      case "Football":
        return CATEGORY_FOOTBALL
      case "Badminton":
        return CATEGORY_BADMINTON
      case "Swim":
        return CATEGORY_SWIM
      case "Basket":
        return CATEGORY_BASKET
    }
  }

  handleLocationPressed() {
    let event: Event = this.state.event
    let url = `https://www.google.com/maps/?q=${event.latitude},${event.longitude}`
    Linking.openURL(url)
  }

  async handleJoinPressed() {
    const eventSourceId = this.props.navigation.getParam("eventSource")
    const eventDestinationId = this.props.navigation.getParam("eventDestination")
    if (this.state.merge) {
      let eventDestination = await EventController.mergeEvent(
        eventSourceId,
        eventDestinationId
      )
      fetch(`http://178.128.219.33:3000/join/${eventDestination.host.player_id}`)
      Alert.alert(
        "Berhasil",
        "Acara anda telah bergabung dengan acara ini! Selamat berolahraga!"
      )
    } else {
      console.log(this.state.event)
      fetch(`http://178.128.219.33:3000/join/${this.state.event.host.player_id}`)
      let isSuccess = await EventController.joinEvent(this.state.event)
      if (isSuccess) {
        Alert.alert("Sukses", "Anda telah bergabung dengan acara ini")
        this.props.navigation.navigate("Tab")
      } else {
        Alert.alert("Maaf", "Acara ini telah penuh")
      }
    }
  }

  handleWhoHasJoinedPressed() {
    this.props.navigation.navigate("EventJoined", { event: this.state.event })
  }

  handleShareButtonPressed() {
    const event: Event = this.state.event
    Share.share({
      title: "Bagikan acara",
      message: `Ayo berolahraga bersama! Dalam acara ${event.title} yang bertempat di ${
        event.location
      }. Ayo bergabung dengan Sportyway. https://play.google.com/`
    })
  }

  render() {
    const event: Event = this.state.event
    const host = event.host as User
    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={styles.container}>
          <Image
            source={this.getCategoryImage()}
            style={styles.categoryImage as ImageStyle}
            resizeMode={"contain"}
          />
          <Text style={styles.title}>{event.title}</Text>
          <View style={styles.detailContainer}>
            <View style={styles.detailItem}>
              <Image
                source={DEFAULT_AVA}
                style={styles.ava as ImageStyle}
                resizeMode={"contain"}
              />
              <View style={styles.detail}>
                <Text>{host.full_name}</Text>
                <View style={styles.detailItem}>
                  <Image source={ICON_PHONE} style={styles.icon as ImageStyle} />
                  <Text>{host.phone_number}</Text>
                </View>
              </View>
            </View>
            <View style={styles.divider} />
            <View style={styles.detailItem}>
              <Image
                source={ICON_GENDER}
                style={styles.icon as ImageStyle}
                resizeMode={"contain"}
              />
              <Text>{event.quota} Orang</Text>
            </View>
            <View style={styles.detailItem}>
              <Image
                source={ICON_TIME}
                style={styles.icon as ImageStyle}
                resizeMode={"contain"}
              />
              <Text>
                {event.date} {event.time}
              </Text>
            </View>
            <TouchableOpacity
              style={styles.detailItem}
              onPress={this.handleLocationPressed}
            >
              <Image
                source={ICON_LOCATION}
                style={styles.icon as ImageStyle}
                resizeMode={"contain"}
              />
              <Text>{event.location}</Text>
            </TouchableOpacity>
            <View style={styles.detailItem}>
              <Image
                source={ICON_MONEY}
                style={styles.icon as ImageStyle}
                resizeMode={"contain"}
              />
              <Text>Rp. {event.price}</Text>
            </View>
            <View style={styles.divider} />
            <View style={styles.socialContainer}>
              <TouchableOpacity style={styles.socialItem}>
                <Image
                  source={ICON_THUMBS}
                  style={styles.socialIcon as ImageStyle}
                  resizeMode={"contain"}
                />
                <Text>Suka</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.socialItem}
                onPress={this.handleShareButtonPressed}
              >
                <Image
                  source={ICON_SHARE}
                  style={styles.socialIcon as ImageStyle}
                  resizeMode={"contain"}
                />
                <Text>Bagikan</Text>
              </TouchableOpacity>
            </View>
          </View>
          <TouchableOpacity
            style={[styles.detailContainer, { marginTop: 10 }]}
            onPress={this.handleWhoHasJoinedPressed}
          >
            <Text>Yang telah bergabung</Text>
            <View style={styles.joinedAvaContainer}>
              <Image
                source={DEFAULT_AVA}
                style={styles.joinedAvaList as ImageStyle}
                resizeMode={"contain"}
              />
              <Image
                source={DEFAULT_AVA}
                style={styles.joinedAvaList as ImageStyle}
                resizeMode={"contain"}
              />
              <Image
                source={DEFAULT_AVA}
                style={styles.joinedAvaList as ImageStyle}
                resizeMode={"contain"}
              />
            </View>
          </TouchableOpacity>
          <View style={styles.spacer} />
        </ScrollView>
        <CustomButton
          label={this.state.merge ? "Gabungkan Acara" : "Gabung"}
          style={styles.joinButton as ImageStyle}
          onPress={this.handleJoinPressed}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },

  categoryImage: {
    width: metrics.DEVICE_WIDTH * 0.4,
    height: metrics.DEVICE_WIDTH * 0.4,
    alignSelf: "center"
  },

  detailContainer: {
    backgroundColor: "white",
    padding: 20,
    borderRadius: 5,
    shadowColor: metrics.SHADOW_COLOR,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 5,
    justifyContent: "center"
  },

  ava: {
    width: 70,
    height: 70,
    borderRadius: 35,
    flex: 1,
    alignSelf: "center"
  },

  detailItem: {
    flexDirection: "row",
    padding: 5,
    marginVertical: 5,
    alignItems: "center"
  },

  detail: {
    flex: 2,
    justifyContent: "center",
    marginLeft: 20
  },

  icon: {
    width: 20,
    height: 20,
    marginRight: 10,
    alignSelf: "center"
  },

  divider: {
    width: metrics.DEVICE_WIDTH * 0.9,
    height: 1,
    backgroundColor: "#EFEFEF",
    alignSelf: "center",
    marginVertical: 5
  },

  socialContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 10
  },

  socialIcon: {
    width: 20,
    height: 20,
    marginRight: 20
  },

  socialItem: {
    flexDirection: "row",
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  joinButton: {
    position: "absolute",
    bottom: 0,
    height: metrics.IS_IPHONE_X ? 65 : 50,
    width: metrics.DEVICE_WIDTH,
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: metrics.IS_IPHONE_X ? 15 : 0
  },

  spacer: {
    height: 70
  },

  joinedAvaList: {
    width: 30,
    height: 30,
    borderRadius: 15,
    marginRight: 5
  },

  joinedAvaContainer: {
    flexDirection: "row",
    marginTop: 10
  },

  title: {
    alignSelf: "center",
    marginTop: 10,
    marginBottom: 20,
    fontWeight: "500",
    fontSize: 19
  }
})
