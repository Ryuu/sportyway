import firebase from "react-native-firebase"
import { DataSnapshot } from "react-native-firebase/database"
import { User } from "./AuthController"
import metrics from "../config/metrics"
import UserController from "./UserController"
import { DeviceEventEmitter } from "react-native"

export interface Event {
  host: User | string
  location: string
  price: number
  quota: number
  time: string
  title: string
  type: string
  latitude: string
  longitude: string
  members: Array<string>
  key: string
  date: string
}

class EventController {
  static getAllEvents = async (): Promise<Array<Event>> => {
    // Get all the events
    let eventSnapshot: DataSnapshot = await firebase
      .database()
      .ref("/event")
      .once("value")
    // Convert data snapshot to array
    let events = metrics.snapshotToArray<Event>(eventSnapshot)
    // Get User data from user id in host field
    events.forEach(async (event: Event) => {
      let userSnapshot: DataSnapshot = await firebase
        .database()
        .ref(`/users/${event.host}`)
        .once("value")
      let userData: User = userSnapshot.val()
      // Change the host to User data
      event.host = userData
      let members = []
      for (let member in event.members) {
        members.push(member)
      }
      event.members = members
    })
    return events
  }

  static getEventFromId = async (id: string): Promise<Event> => {
    let eventSnapshot: DataSnapshot = await firebase
      .database()
      .ref(`/event/${id}`)
      .once("value")
    let eventData: Event = eventSnapshot.val()
    let hostSnapshot: DataSnapshot = await firebase
      .database()
      .ref(`/users/${eventData.host}`)
      .once("value")
    let hostData: User = hostSnapshot.val()
    eventData.host = hostData
    return eventData
  }

  static mergeEvent = async (
    event_source_id: string,
    event_destination_id: string
  ): Promise<Event> => {
    let eventSource: Event = await EventController.getEventFromId(event_source_id)
    let eventDestination = await EventController.getEventFromId(event_destination_id)
    let eventDestinationRef = firebase.database().ref(`event/${event_destination_id}`)
    console.log("EVENT DESTINATION", event_destination_id)
    console.log(eventSource.members)
    for (let member in eventSource.members) {
      console.log("setting new members", member)
      await eventDestinationRef
        .child("members")
        .child(member)
        .set(true)
    }
    return eventDestination
  }

  static postEvent = async (event: Event): Promise<boolean> => {
    firebase
      .database()
      .ref("/event")
      .push(event)
    let uid = UserController.getUserId()
    let userRef = firebase.database().ref(`users/${uid}`)
    let user: User = await UserController.getUserData()
    userRef.child("events_made").set(++user.events_made)
    userRef.child("reputation").set((user.reputation += 10))
    DeviceEventEmitter.emit("shouldRefreshData")
    return true
  }

  static joinEvent = async (event: Event): Promise<boolean> => {
    if (event.members.length < event.quota) {
      let eventRef = firebase.database().ref(`/event/${event.key}`)
      await eventRef
        .child("members")
        .child(UserController.getUserId())
        .set(true)
      let user: User = await UserController.getUserData()
      let userRef = firebase.database().ref(`users/${UserController.getUserId()}`)
      userRef.child("events_joined").set(++user.events_joined)
      userRef.child("reputation").set((user.reputation += 5))
      DeviceEventEmitter.emit("shouldRefreshData")
      return true
    } else {
      return false
    }
  }

  static getAddressFromLatLng = async (lat: number, lng: number): Promise<string> => {
    let address = await fetch(
      `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=AIzaSyAAk5AQietyDTn99hx1S98ujv01yQfRkJ0`
    )
    address = await address.json()
    console.log(address)
    return address.results[0].formatted_address
  }
}

export default EventController
