import React from "react"
import {
  StyleSheet,
  Image,
  ScrollView,
  Text,
  View,
  Keyboard,
  DatePickerAndroid,
  DatePickerIOS,
  TimePickerAndroid,
  ActivityIndicator,
  ImageStyle
} from "react-native"
import { NavigationStackScreenOptions, NavigationScreenProp } from "react-navigation"
import Modal from "react-native-modal"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import RNGooglePlaces from "react-native-google-places"

import CustomTextInput from "../components/CustomTextInput"
import CustomButton from "../components/CustomButton"
import metrics from "../config/metrics"
import EventController, { Event } from "../controller/EventController"
import AuthController from "../controller/AuthController"
import UserController from "../controller/UserController"

const CATEGORY_JOGGING = require("../../assets/sport_category_jogging.png")
const CATEGORY_FOOTBALL = require("../../assets/sport_category_football.png")
const CATEGORY_BADMINTON = require("../../assets/sport_category_badminton.png")
const CATEGORY_SWIM = require("../../assets/sport_category_swim.png")
const CATEGORY_BASKET = require("../../assets/sport_category_basket.png")

interface Props {
  navigation: NavigationScreenProp<any, any>
}

interface State {
  isDateModalVisible: boolean
  isTimeModalVisible: boolean
  selectedDate: Date
  date: string
  title: string
  location: string
  price: number
  quota: number
  host: string
  time: string
  type: string
  latitude: string
  longitude: string
  isLoading: boolean
}

export default class CreateEvent extends React.Component<Props, State> {
  static navigationOptions: NavigationStackScreenOptions = {
    title: "Create Event"
  }

  constructor(props: Props) {
    super(props)
    this.showDate = this.showDate.bind(this)
    this.showTime = this.showTime.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.handleDateChangeIOS = this.handleDateChangeIOS.bind(this)
    this.handleTimeChangeIOS = this.handleTimeChangeIOS.bind(this)
    this.handleTitleInputChange = this.handleTitleInputChange.bind(this)
    this.handlePriceInputChange = this.handlePriceInputChange.bind(this)
    this.handleQuotaInputChange = this.handleQuotaInputChange.bind(this)
    this.handleLocationFocus = this.handleLocationFocus.bind(this)
    this.handleCreateEventButtonPressed = this.handleCreateEventButtonPressed.bind(this)
  }

  state = {
    isDateModalVisible: false,
    isTimeModalVisible: false,
    selectedDate: new Date(),
    date: "",
    title: "",
    location: "",
    price: 0,
    quota: 0,
    host: "",
    time: "",
    type: "",
    latitude: "",
    longitude: "",
    isLoading: false
  }

  componentDidMount() {
    // Get the category selection from screen before this
    const param = this.props.navigation.getParam("category")
    this.setState({ type: param })
  }

  // Get the corresponding category image based on user selection before
  getCategoryImage() {
    switch (this.state.type) {
      case "Jogging":
        return CATEGORY_JOGGING
      case "Football":
        return CATEGORY_FOOTBALL
      case "Badminton":
        return CATEGORY_BADMINTON
      case "Swim":
        return CATEGORY_SWIM
      case "Basket":
        return CATEGORY_BASKET
    }
  }

  // To add 0 in front of value to match string format
  normalizeDate(value: number): string {
    if (value < 10) {
      return "0" + String(value)
    } else {
      return String(value)
    }
  }

  handleDateChangeIOS(date: Date) {
    let dateString: string
    // Normalize the date first
    let day = this.normalizeDate(date.getDate())
    let month = metrics.GET_MONTH_NAME(date.getMonth())
    // Combine the date
    dateString = day + " " + month + " " + date.getFullYear()
    this.setState({ date: dateString, selectedDate: date })
  }

  handleTimeChangeIOS(date: Date) {
    let timeString: string
    // Normalize the hour first
    let hour = this.normalizeDate(date.getHours())
    // Normalize the minute first
    let minute = this.normalizeDate(date.getMinutes())
    // Combine the time
    timeString = hour + " : " + minute
    this.setState({ time: timeString, selectedDate: date })
  }

  async showDate() {
    // Dismiss the keyboard first
    Keyboard.dismiss()
    // Use built in date picker in Android
    if (metrics.OS === "android") {
      try {
        let { action, year, month, day, ...other } = await DatePickerAndroid.open({
          // Initial selection is now
          date: new Date()
        })
        if (action !== DatePickerAndroid.dismissedAction) {
          let date = ""
          if (day) {
            // Directly add the day to the date string
            date += this.normalizeDate(day) + " "
          }
          if (month) {
            // Convert month number to name
            let monthName = metrics.GET_MONTH_NAME(month)
            // // Directly add the month to the date string
            date += monthName + " "
          }
          if (year) {
            // Directly add the year to the date string
            date += year
          }
          this.setState({ date: date })
        }
      } catch (err) {
        alert(err)
      }
    } else {
      // Call onChange callback first to handle if user doesn't change the date
      this.handleDateChangeIOS(new Date())
      // Use modal in iOS because the picker sucks
      this.setState({ isDateModalVisible: true })
    }
  }

  async showTime() {
    // Dismiss the keyboard first
    Keyboard.dismiss()
    // Use the built in time picker in Android
    if (metrics.OS === "android") {
      try {
        let { action, hour, minute } = await TimePickerAndroid.open({})
        if (action !== TimePickerAndroid.dismissedAction) {
          let time = ""
          if (hour) {
            // Directly add the hour to the date string
            time += this.normalizeDate(hour) + " : "
          }
          if (minute) {
            // Directly add the minute to the date string
            time += this.normalizeDate(minute)
          }
          this.setState({ time: time })
        }
      } catch (err) {
        alert(err)
      }
    } else {
      // Call onChange callback first to handle if user doesn't change the time
      this.handleTimeChangeIOS(new Date())
      // Use modal in iOS because the picker sucks
      this.setState({ isTimeModalVisible: true })
    }
  }

  async handleLocationFocus(): Promise<void> {
    Keyboard.dismiss()
    const place = await RNGooglePlaces.openPlacePickerModal()
    console.log(place)
    const address = await EventController.getAddressFromLatLng(
      place.latitude,
      place.longitude
    )
    console.log(address)
    this.setState({
      location: address,
      latitude: place.latitude,
      longitude: place.longitude
    })
  }

  closeModal() {
    // Close all modal
    this.setState({ isDateModalVisible: false, isTimeModalVisible: false })
  }

  handleTitleInputChange(value: string): void {
    this.setState({ title: value })
  }

  handlePriceInputChange(value: string): void {
    this.setState({ price: Number(value) })
  }

  handleQuotaInputChange(value: string): void {
    this.setState({ quota: Number(value) })
  }

  async handleCreateEventButtonPressed(): Promise<void> {
    this.setState({ isLoading: true })
    let key = UserController.getUserId()
    let host = {} as any
    host[key] = true
    let event: Event = {
      host: UserController.getUserId(),
      latitude: this.state.latitude,
      location: this.state.location,
      longitude: this.state.longitude,
      price: this.state.price,
      quota: this.state.quota,
      time: this.state.time,
      title: this.state.title,
      type: this.state.type,
      key: "",
      members: host,
      date: this.state.date
    }
    try {
      await EventController.postEvent(event)
      this.props.navigation.navigate("Tab")
    } catch (err) {
      alert(err.message)
    } finally {
      this.setState({ isLoading: false })
    }
  }

  render() {
    // Bug confirmed from TS team about style error below but everything works fine nonetheless
    // I know it's annoying but ¯\_(ツ)_/¯
    return (
      <KeyboardAwareScrollView
        style={styles.container}
        contentContainerStyle={{ alignItems: "center" }}
      >
        <Image
          source={this.getCategoryImage()}
          style={styles.categoryIcon as ImageStyle}
          resizeMode={"contain"}
        />
        <CustomTextInput
          placeholder={"Judul Acara"}
          onChangeText={this.handleTitleInputChange}
        />
        <CustomTextInput
          placeholder={"Tanggal Acara"}
          onFocus={this.showDate}
          value={this.state.date}
        />
        <CustomTextInput
          placeholder={"Waktu Acara"}
          onFocus={this.showTime}
          value={this.state.time}
        />
        <CustomTextInput
          placeholder={"Lokasi Acara"}
          onFocus={this.handleLocationFocus}
          value={this.state.location}
        />
        <CustomTextInput
          placeholder={"Harga"}
          onChangeText={this.handlePriceInputChange}
        />
        <CustomTextInput
          placeholder={"Quota"}
          keyboardType={"numeric"}
          onChangeText={this.handleQuotaInputChange}
        />
        {this.state.isLoading ? (
          <ActivityIndicator />
        ) : (
          <CustomButton
            label={"Buat Acara"}
            onPress={this.handleCreateEventButtonPressed}
          />
        )}
        <Modal isVisible={this.state.isDateModalVisible} style={styles.bottomModal}>
          <View style={styles.modalContent}>
            <Text>Select date</Text>
            <DatePickerIOS
              date={this.state.selectedDate}
              onDateChange={this.handleDateChangeIOS}
              mode={"date"}
            />
            <CustomButton label={"Tutup"} onPress={this.closeModal} />
          </View>
        </Modal>
        <Modal isVisible={this.state.isTimeModalVisible} style={styles.bottomModal}>
          <View style={styles.modalContent}>
            <Text>Select time</Text>
            <DatePickerIOS
              date={this.state.selectedDate}
              mode={"time"}
              onDateChange={this.handleTimeChangeIOS}
            />
            <CustomButton label={"Tutup"} onPress={this.closeModal} />
          </View>
        </Modal>
      </KeyboardAwareScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20
  },

  categoryIcon: {
    width: 100,
    height: 100,
    marginBottom: 20
  },

  bottomModal: {
    justifyContent: "flex-end",
    margin: 0
  },

  modalContent: {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  }
})
