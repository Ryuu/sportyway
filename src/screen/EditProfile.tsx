import React from "react"
import {
  View,
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
  ImageSourcePropType,
  ActivityIndicator,
  ImageStyle
} from "react-native"
import { NavigationStackScreenOptions, NavigationScreenProp } from "react-navigation"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import ImagePicker from "react-native-image-picker"
import CustomTextInput from "../components/CustomTextInput"
import CustomButton from "../components/CustomButton"
import { User } from "../controller/AuthController"
import UserController from "../controller/UserController"

const DEFAULT_AVA = require("../../assets/ava_default.png")

interface Props {
  navigation: NavigationScreenProp<any, any>
}

interface State {
  userAva: ImageSourcePropType
  user: User
  fullName: string
  phoneNumber: string
  address: string
  isLoading: boolean
}

export default class EditProfile extends React.Component<Props, State> {
  static navigationOptions: NavigationStackScreenOptions = {
    title: "Ubah Profil"
  }

  state = {
    userAva: DEFAULT_AVA,
    user: this.props.navigation.getParam("userData"),
    fullName: this.props.navigation.getParam("userData").full_name,
    phoneNumber: this.props.navigation.getParam("userData").phone_number,
    address: this.props.navigation.getParam("userData").address,
    isLoading: false
  }

  constructor(props: Props) {
    super(props)
    this.openImagePicker = this.openImagePicker.bind(this)
    this.handleFullNameInputChanged = this.handleFullNameInputChanged.bind(this)
    this.handleAddressInputChanged = this.handleAddressInputChanged.bind(this)
    this.handlePhoneNumberInputChanged = this.handlePhoneNumberInputChanged.bind(this)
    this.handleSaveButtonPressed = this.handleSaveButtonPressed.bind(this)
  }

  openImagePicker(): void {
    ImagePicker.launchImageLibrary(
      {
        title: "Select Avatar"
      },
      response => {
        this.setState({ userAva: response })
      }
    )
  }

  handleFullNameInputChanged(value: string): void {
    this.setState({ fullName: value })
  }

  handlePhoneNumberInputChanged(value: string): void {
    this.setState({ phoneNumber: value })
  }

  handleAddressInputChanged(value: string): void {
    this.setState({ address: value })
  }

  async handleSaveButtonPressed(): Promise<void> {
    this.setState({ isLoading: true })
    const user: User = this.state.user
    user.full_name = this.state.fullName
    user.address = this.state.address
    user.phone_number = this.state.phoneNumber
    try {
      await UserController.editProfile(user)
      this.props.navigation.goBack(null)
    } catch (err) {
      alert(err.message)
    } finally {
      this.setState({ isLoading: true })
    }
  }

  render() {
    return (
      <KeyboardAwareScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        <TouchableOpacity onPress={this.openImagePicker}>
          <Image source={this.state.userAva} style={styles.ava as ImageStyle} />
        </TouchableOpacity>
        <CustomTextInput
          placeholder={"Nama Lengkap"}
          defaultValue={this.state.fullName}
          onChangeText={this.handleFullNameInputChanged}
        />
        <CustomTextInput
          placeholder={"Nomor Telepon"}
          keyboardType={"number-pad"}
          defaultValue={this.state.phoneNumber}
          onChangeText={this.handlePhoneNumberInputChanged}
        />
        <CustomTextInput
          placeholder={"Alamat"}
          defaultValue={this.state.address}
          onChangeText={this.handleAddressInputChanged}
        />
        {this.state.isLoading ? (
          <ActivityIndicator />
        ) : (
          <CustomButton label={"Simpan"} onPress={this.handleSaveButtonPressed} />
        )}
      </KeyboardAwareScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20
  },

  contentContainer: {
    justifyContent: "center",
    alignItems: "center"
  },

  ava: {
    width: 100,
    height: 100,
    borderRadius: 50,
    alignSelf: "center",
    marginBottom: 20
  }
})
