import React from "react"
import { createStackNavigator, createBottomTabNavigator } from "react-navigation"
import SplashScreen from "react-native-splash-screen"
import { RNFirebase } from "react-native-firebase"
import OneSignal from "react-native-onesignal"

// Import screens
import Login from "./src/screen/Login"
import Register from "./src/screen/Register"
import EventDetail from "./src/screen/EventDetail"
import ChooseCategory from "./src/screen/ChooseCategory"
import CreateEvent from "./src/screen/CreateEvent"
import InputUserDetail from "./src/screen/InputUserDetail"
import EditProfile from "./src/screen/EditProfile"
import EventJoined from "./src/screen/EventJoined"

// Tab screens
import Map from "./src/screen/Map"
import Events from "./src/screen/Events"
import Profile from "./src/screen/Profile"

import metrics from "./src/config/metrics"
import AuthController from "./src/controller/AuthController"
import EventController from "./src/controller/EventController"
import { DeviceEventEmitter } from "react-native"

interface State {
  isUserLoggedIn: RNFirebase.User | null
}

export default class App extends React.Component<any, State> {
  state = {
    isUserLoggedIn: null
  }

  async componentDidMount() {
    OneSignal.init("f835ba28-90ba-478a-b0d3-49bfd3e354df")
    OneSignal.addEventListener("received", this.onReceived)
    OneSignal.addEventListener("opened", this.onOpened)
    try {
      let session = await AuthController.getUserSession()
      await this.setState({ isUserLoggedIn: session })
    } catch (err) {
      alert(err.message)
    } finally {
      SplashScreen.hide()
    }
  }

  // componentWillUnmount() {
  //   OneSignal.removeEventListener("received", this.onReceived)
  //   OneSignal.removeEventListener("opened", this.onOpened)
  // }

  onReceived(notification: any) {
    console.log("Notification received: ", notification)
  }

  async onOpened(openResult: any) {
    console.log("Message: ", openResult.notification.payload.body)
    console.log("Data: ", openResult.notification.payload.additionalData)
    console.log("isActive: ", openResult.notification.isAppInFocus)
    console.log("openResult: ", openResult)
    const data = openResult.notification.payload.additionalData
    if (data.event_id) {
      let event = await EventController.getEventFromId(data.event_id)
      DeviceEventEmitter.emit("notificationReceived", {
        event: event,
        event_source: data.event_id_source,
        event_destination: data.event_id
      })
    }
  }

  render() {
    let session: boolean
    if (this.state.isUserLoggedIn) {
      session = true
    } else {
      session = false
    }
    const Nav = Navigator(session)
    return <Nav />
  }
}

const TabBar = createBottomTabNavigator(
  {
    Map: { screen: Map },
    Events: { screen: Events },
    Profile: { screen: Profile }
  },
  {
    tabBarOptions: {
      activeTintColor: metrics.PRIMARY_COLOR,
      inactiveTintColor: "#909090",
      showIcon: true,
      style: {
        backgroundColor: "white"
      }
    },
    tabBarPosition: "bottom",
    animationEnabled: false,
    swipeEnabled: false
  }
)

const Navigator = (signedIn = false) =>
  createStackNavigator(
    {
      Login: { screen: Login },
      Register: { screen: Register },
      ChooseCategory: { screen: ChooseCategory },
      EventDetail: { screen: EventDetail },
      CreateEvent: { screen: CreateEvent },
      InputUserDetail: { screen: InputUserDetail },
      EditProfile: { screen: EditProfile },
      EventJoined: { screen: EventJoined },
      Tab: {
        screen: TabBar,
        navigationOptions: {
          title: "Sportyway",
          headerLeft: null
        }
      }
    },
    {
      navigationOptions: {
        headerStyle: {
          backgroundColor: metrics.PRIMARY_COLOR
        },
        headerTintColor: "white"
      },
      initialRouteName: signedIn ? "Tab" : "Login"
    }
  )
