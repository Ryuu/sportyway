import React from "react"
import {
  StyleSheet,
  View,
  Text,
  Image,
  Alert,
  ScrollView,
  ActivityIndicator,
  DeviceEventEmitter,
  ImageStyle
} from "react-native"
import {
  NavigationTabScreenOptions,
  StackActions,
  NavigationActions,
  NavigationScreenProp
} from "react-navigation"
import metrics from "../config/metrics"
import CustomButton from "../components/CustomButton"
import AuthController, { User } from "../controller/AuthController"
import UserController from "../controller/UserController"

const ICON_ACTIVE = require("../../assets/ic_person_active.png")
const ICON_INACTIVE = require("../../assets/ic_person_inactive.png")
const CHARACTER = require("../../assets/char_level_1.png")
const DEFAULT_AVA = require("../../assets/ava_default.png")
const ICON_EMAIL = require("../../assets/ic_email.png")
const ICON_PHONE = require("../../assets/ic_phone.png")
const ICON_LOCATION = require("../../assets/ic_location_profile.png")
const ICON_BIKE = require("../../assets/ic_bike.png")

interface Props {
  navigation: NavigationScreenProp<any, any>
}

interface State {
  userData: User
  email: string | null
}

export default class Profile extends React.Component<Props, State> {
  static navigationOptions: NavigationTabScreenOptions = {
    title: "Profil",
    tabBarIcon: ({ focused }) => {
      switch (focused) {
        case true:
          return (
            <Image
              source={ICON_ACTIVE}
              resizeMode={"contain"}
              style={metrics.TAB_BAR_ICON_STYLE}
            />
          )
        case false:
          return (
            <Image
              source={ICON_INACTIVE}
              resizeMode={"contain"}
              style={metrics.TAB_BAR_ICON_STYLE}
            />
          )
      }
    }
  }

  state = {
    userData: {
      full_name: "",
      phone_number: "",
      gender: "",
      sport_type: "",
      address: "",
      events_made: 0,
      events_joined: 0,
      reputation: 0,
      player_id: ""
    },
    email: ""
  }

  constructor(props: Props) {
    super(props)
    this.logout = this.logout.bind(this)
    this.handleEditProfileButtonPressed = this.handleEditProfileButtonPressed.bind(this)
  }

  async componentDidMount() {
    let userData = await UserController.getUserData()
    let email = await UserController.getUserEmail()
    this.setState({ userData: userData, email: email })
  }

  componentWillMount() {
    DeviceEventEmitter.addListener("shouldRefreshData", () => {
      this.componentDidMount()
    })
  }

  logout() {
    Alert.alert("Keluar", "Anda yakin ingin keluar?", [
      {
        text: "Tidak"
      },
      {
        text: "Ya",
        style: "destructive",
        onPress: () => {
          AuthController.logout(() => {
            // Make a reset action to clear the stack screens and navigate to the desired screen
            const resetAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: "Login" })]
            })
            // Call the reset action
            this.props.navigation.dispatch(resetAction)
          })
        }
      }
    ])
  }

  handleEditProfileButtonPressed() {
    this.props.navigation.navigate("EditProfile", { userData: this.state.userData })
  }

  render() {
    if (this.state.userData.full_name !== "") {
      return (
        <ScrollView style={styles.scrollContainer}>
          <View style={styles.container}>
            <Text style={styles.level}>Level 1</Text>
            <Image
              source={CHARACTER}
              style={styles.character as ImageStyle}
              resizeMode={"cover"}
            />
            <Text style={styles.name}>{this.state.userData.full_name}</Text>
            <View style={styles.statsContainer}>
              <View style={styles.statsItem}>
                <Text>Acara Dibuat</Text>
                <Text style={styles.statsValue}>{this.state.userData.events_made}</Text>
              </View>
              <View style={styles.statsItem}>
                <Text>Acara Diikuti</Text>
                <Text style={styles.statsValue}>{this.state.userData.events_joined}</Text>
              </View>
              <View style={styles.statsItem}>
                <Text>Reputasi</Text>
                <Text style={styles.statsValue}>{this.state.userData.reputation}</Text>
              </View>
            </View>
            <View style={styles.profileContainer}>
              <Image
                source={DEFAULT_AVA}
                style={styles.ava as ImageStyle}
                resizeMode={"contain"}
              />
              <View style={styles.detailContainer}>
                <View style={styles.detailItemContainer}>
                  <Image
                    source={ICON_EMAIL}
                    style={styles.icon as ImageStyle}
                    resizeMode={"contain"}
                  />
                  <Text>{this.state.email}</Text>
                </View>
                <View style={styles.detailItemContainer}>
                  <Image
                    source={ICON_PHONE}
                    style={styles.icon as ImageStyle}
                    resizeMode={"contain"}
                  />
                  <Text>{this.state.userData.phone_number}</Text>
                </View>
                <View style={styles.detailItemContainer}>
                  <Image
                    source={ICON_LOCATION}
                    style={styles.icon as ImageStyle}
                    resizeMode={"contain"}
                  />
                  <Text>{this.state.userData.address}</Text>
                </View>
                <View style={styles.detailItemContainer}>
                  <Image
                    source={ICON_BIKE}
                    style={styles.icon as ImageStyle}
                    resizeMode={"contain"}
                  />
                  <Text>{this.state.userData.sport_type}</Text>
                </View>
              </View>
            </View>
            <CustomButton
              label={"Ubah Profil"}
              onPress={this.handleEditProfileButtonPressed}
            />
            <CustomButton
              label={"Keluar"}
              backgroundColor={metrics.DANGER_COLOR}
              onPress={this.logout}
            />
          </View>
        </ScrollView>
      )
    } else {
      return (
        <View style={styles.container}>
          <ActivityIndicator size={"large"} />
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1
  },

  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 30
  },

  level: {
    position: "absolute",
    top: 20,
    left: 20
  },

  character: {
    width: metrics.DEVICE_WIDTH * 0.3,
    height: metrics.DEVICE_WIDTH * 0.3
  },

  name: {
    fontSize: 25,
    marginVertical: 20
  },

  statsContainer: {
    flexDirection: "row",
    backgroundColor: "white",
    padding: 20,
    shadowColor: metrics.SHADOW_COLOR,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 5
  },

  statsItem: {
    alignItems: "center",
    flex: 1
  },

  statsValue: {
    fontSize: 30,
    marginTop: 10
  },

  profileContainer: {
    backgroundColor: "white",
    flexDirection: "row",
    padding: 20,
    shadowColor: metrics.SHADOW_COLOR,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 5,
    marginVertical: 20
  },

  ava: {
    width: 50,
    height: 50,
    borderRadius: 25,
    flex: 1,
    alignSelf: "center"
  },

  detailContainer: {
    flex: 1,
    justifyContent: "center",
    marginLeft: 20
  },

  detailItemContainer: {
    flexDirection: "row",
    marginVertical: 5
  },

  icon: {
    width: 15,
    height: 15,
    marginRight: 10,
    alignSelf: "center"
  }
})
