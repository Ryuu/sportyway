import { AsyncStorage, Alert } from "react-native"
import firebase, { RNFirebase } from "react-native-firebase"

export interface User {
  full_name: string
  phone_number: string
  gender: string
  sport_type: string
  address: string
  events_made: number
  events_joined: number
  reputation: number
  player_id: string
}

class AuthController {
  static setUserSessionLoggedIn = async (
    email: string | null,
    password: string,
    uid: string
  ): Promise<boolean> => {
    try {
      AsyncStorage.setItem("sessionemail", email!)
      AsyncStorage.setItem("sessionpassword", password)
      AsyncStorage.setItem("sessionuid", uid)
    } catch (err) {
      throw err
    }
    return true
  }

  static getUserSession = async (): Promise<RNFirebase.User | null> => {
    let email = await AsyncStorage.getItem("sessionemail")
    let password = await AsyncStorage.getItem("sessionpassword")
    if (email) {
      if (password) {
        let user = firebase.auth().currentUser
        if (user) {
          return user
        }
        await AuthController.login(email, password)
        user = firebase.auth().currentUser
        return user
      }
    }
    return null
  }

  static login = async (email: string, password: string) => {
    try {
      let userCredential: RNFirebase.UserCredential = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
      if (userCredential) {
        AuthController.setUserSessionLoggedIn(
          userCredential.user.email,
          password,
          userCredential.user.uid
        )
      }
      return userCredential.user.uid
    } catch (err) {
      throw err
    }
  }

  static logout = async (cb: Function) => {
    await AsyncStorage.multiRemove(["sessionemail", "sessionpassword"])
    cb()
  }

  static register = async (email: string, password: string) => {
    try {
      await firebase.auth().createUserWithEmailAndPassword(email, password)
      return true
    } catch (err) {
      throw err
    }
  }

  static postUserDetails = async (user: User) => {
    let currentUser = firebase.auth().currentUser
    let newUserRef = firebase
      .database()
      .ref("/users")
      .child(currentUser!.uid)
    await newUserRef.set(user)
  }
}

export default AuthController
