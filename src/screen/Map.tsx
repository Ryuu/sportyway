import React, { createRef } from "react"
import {
  View,
  StyleSheet,
  Image,
  StatusBar,
  PermissionsAndroid,
  GeolocationReturnType,
  Alert,
  DeviceEventEmitter
} from "react-native"
import { NavigationTabScreenOptions, NavigationScreenProp } from "react-navigation"
import MapView, { Region, Marker } from "react-native-maps"
import OneSignal from "react-native-onesignal"
import metrics from "../config/metrics"
import FloatingActionButton from "../components/FloatingActionButton"
import EventController, { Event } from "../controller/EventController"
import UserController from "../controller/UserController"

const ICON_ACTIVE = require("../../assets/ic_map_active.png")
const ICON_INACTIVE = require("../../assets/ic_map_inactive.png")

const MARKER_BASKET = require("../../assets/marker_basket.png")
const MARKER_BADMINTON = require("../../assets/marker_badminton.png")
const MARKER_FOOTBALL = require("../../assets/marker_football.png")
const MARKER_JOGGING = require("../../assets/marker_jogging.png")
const MARKER_SWIM = require("../../assets/marker_swim.png")

interface Props {
  navigation: NavigationScreenProp<any, any>
}

interface State {
  currentLocation: Region
  isMapReady: boolean
  events: Array<Event>
}

export default class Map extends React.Component<Props, State> {
  static navigationOptions: NavigationTabScreenOptions = {
    title: "Peta",
    tabBarIcon: ({ focused }) => {
      switch (focused) {
        case true:
          return (
            <Image
              source={ICON_ACTIVE}
              resizeMode={"contain"}
              style={metrics.TAB_BAR_ICON_STYLE}
            />
          )
        case false:
          return (
            <Image
              source={ICON_INACTIVE}
              resizeMode={"contain"}
              style={metrics.TAB_BAR_ICON_STYLE}
            />
          )
      }
    }
  }

  state = {
    currentLocation: {
      latitude: 37.78825,
      longitude: -122.4324,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421
    },
    isMapReady: false,
    events: []
  }

  // Reference variable for Map View
  private mapRef = createRef<MapView>()

  constructor(props: Props) {
    super(props)
    this.handleAddEventButtonPressed = this.handleAddEventButtonPressed.bind(this)
    this.onMapReady = this.onMapReady.bind(this)
    this.requestLocationPermission()
  }

  async componentDidMount() {
    OneSignal.configure()
    OneSignal.addEventListener("ids", this.onIds)
    try {
      let events = await EventController.getAllEvents()
      await this.setState({ events: events })
    } catch (err) {
      alert(err.message)
    } finally {
      this.setState({ isMapReady: true })
    }
    console.log(this.state)
  }

  componentWillMount() {
    DeviceEventEmitter.addListener("shouldRefreshData", () => {
      this.componentDidMount()
    })
    DeviceEventEmitter.addListener("notificationReceived", event => {
      // console.log(event)
      this.props.navigation.navigate("EventDetail", {
        event: event.event,
        merge: true,
        eventSource: event.event_source,
        eventDestination: event.event_destination
      })
    })
  }

  onIds(device: any) {
    console.log("Device info: ", device)
    UserController.setPlayerId(device.userId)
  }

  // Request location permission for android
  async requestLocationPermission() {
    // iOS already handled their permission on OS level
    if (metrics.OS === "android") {
      if (
        !PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
      ) {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: "Sportyway",
              message: "Sportyway needs to access your location to show nearby events"
            }
          )
          if (granted === PermissionsAndroid.RESULTS.DENIED) {
            Alert.alert(
              "Warning",
              "You have denied location permissions, app may not work as intended"
            )
          }
        } catch (err) {
          console.warn(err)
        }
      }
    }
  }

  handleAddEventButtonPressed() {
    this.props.navigation.navigate("ChooseCategory")
  }

  handleMarkerPressed(event: Event) {
    this.props.navigation.navigate("EventDetail", { event: event })
  }

  onMapReady() {
    navigator.geolocation.getCurrentPosition((position: GeolocationReturnType) => {
      // Convert GeolocationReturnType to Region to be usable in Map View
      let region: Region = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      }
      const mapView = this.mapRef.current
      // Move the map to current position
      if (mapView) {
        mapView.animateToRegion(region)
        this.setState({ currentLocation: region })
      }
    })
  }

  getMarkerImage(type: string) {
    switch (type) {
      case "Basket":
        return MARKER_BASKET
      case "Badminton":
        return MARKER_BADMINTON
      case "Jogging":
        return MARKER_JOGGING
      case "Swim":
        return MARKER_SWIM
      case "Football":
        return MARKER_FOOTBALL
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle={"light-content"} />
        <MapView
          ref={this.mapRef}
          provider={"google"}
          showsMyLocationButton={true}
          showsUserLocation={true}
          onMapReady={this.onMapReady}
          region={this.state.currentLocation}
          style={[styles.container, { margin: this.state.isMapReady ? 1 : 0 }]}
          loadingEnabled={true}
          loadingIndicatorColor="#666666"
          loadingBackgroundColor="#eeeeee"
          showsCompass={true}
        >
          {this.state.isMapReady
            ? this.state.events.map((event: Event) => {
                return (
                  <Marker
                    coordinate={{
                      latitude: Number(event.latitude),
                      longitude: Number(event.longitude)
                    }}
                    onPress={this.handleMarkerPressed.bind(this, event)}
                    image={this.getMarkerImage(event.type)}
                  />
                )
              })
            : null}
        </MapView>
        <FloatingActionButton onPress={this.handleAddEventButtonPressed} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})
