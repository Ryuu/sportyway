import React from "react"
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  ImageSourcePropType
} from "react-native"
import metrics from "../config/metrics"

interface Props {
  onPress?: () => void
  name: string
  image: ImageSourcePropType
}

export default class CategoryItem extends React.Component<Props, any> {
  render() {
    return (
      <TouchableOpacity style={styles.container} onPress={this.props.onPress}>
        <Image source={this.props.image} style={styles.image} resizeMode={"contain"} />
        <Text>{this.props.name}</Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: metrics.SHADOW_COLOR,
    width: metrics.DEVICE_WIDTH * 0.5,
    height: metrics.DEVICE_WIDTH * 0.5,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center"
  },

  image: {
    width: 70,
    height: 70,
    marginBottom: 20
  }
})
