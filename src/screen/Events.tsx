import React from "react"
import { View, StyleSheet, Image, FlatList, DeviceEventEmitter } from "react-native"
import { NavigationScreenProp, NavigationTabScreenOptions } from "react-navigation"

import metrics from "../config/metrics"
import EventItem from "../components/EventItem"
import EventController, { Event } from "../controller/EventController"

const ICON_ACTIVE = require("../../assets/ic_event_active.png")
const ICON_INACTIVE = require("../../assets/ic_event_inactive.png")

interface Props {
  navigation: NavigationScreenProp<any, any>
}

interface State {
  events: Array<Event>
  isDataLoading: boolean
}

export default class Events extends React.Component<Props, State> {
  static navigationOptions: NavigationTabScreenOptions = {
    title: "Acara",
    tabBarIcon: ({ focused }) => {
      switch (focused) {
        case true:
          return (
            <Image
              source={ICON_ACTIVE}
              resizeMode={"contain"}
              style={metrics.TAB_BAR_ICON_STYLE}
            />
          )
        case false:
          return (
            <Image
              source={ICON_INACTIVE}
              resizeMode={"contain"}
              style={metrics.TAB_BAR_ICON_STYLE}
            />
          )
      }
    }
  }

  constructor(props: Props) {
    super(props)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  state = {
    events: [],
    isDataLoading: false
  }

  async componentDidMount() {
    this.setState({ isDataLoading: true })
    try {
      let events = await EventController.getAllEvents()
      this.setState({ events: events })
    } catch (err) {
      alert(err.message)
    } finally {
      this.setState({ isDataLoading: false })
    }
  }

  componentWillMount() {
    DeviceEventEmitter.addListener("shouldRefreshData", () => {
      this.componentDidMount()
    })
  }

  handleEventItemPressed(event: Event) {
    this.props.navigation.navigate("EventDetail", { event: event })
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.events}
          renderItem={({ item }) => (
            <EventItem onPress={this.handleEventItemPressed.bind(this, item)} {...item} />
          )}
          onRefresh={this.componentDidMount}
          refreshing={this.state.isDataLoading}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  }
})
