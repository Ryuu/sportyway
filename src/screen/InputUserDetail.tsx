import React from "react"
import { View, StyleSheet, Text } from "react-native"
import { NavigationStackScreenOptions, NavigationScreenProp } from "react-navigation"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import CustomTextInput from "../components/CustomTextInput"
import CustomButton from "../components/CustomButton"
import AuthController, { User } from "../controller/AuthController"

interface Props {
  navigation: NavigationScreenProp<any, any>
}

interface State {
  fullName: string
  phoneNumber: string
  gender: string
  favSport: string
  address: string
  isSubmittingDetail: boolean
}

export default class InputUserDetail extends React.Component<Props, State> {
  static navigationOptions: NavigationStackScreenOptions = {
    title: "Detail User"
  }

  state = {
    fullName: "",
    phoneNumber: "",
    gender: "",
    favSport: "",
    address: "",
    isSubmittingDetail: false
  }

  constructor(props: Props) {
    super(props)
    this.handleSubmitButtonPressed = this.handleSubmitButtonPressed.bind(this)
    this.handleFullNameInputChange = this.handleFullNameInputChange.bind(this)
    this.handlePhoneNumberInputChange = this.handlePhoneNumberInputChange.bind(this)
    this.handleGenderInputChange = this.handleGenderInputChange.bind(this)
    this.handleFavSportInputChange = this.handleFavSportInputChange.bind(this)
    this.handleAddressInputChange = this.handleAddressInputChange.bind(this)
  }

  async handleSubmitButtonPressed(): Promise<void> {
    this.setState({ isSubmittingDetail: true })
    let user: User = {
      full_name: this.state.fullName,
      phone_number: this.state.phoneNumber,
      gender: this.state.gender,
      sport_type: this.state.favSport,
      address: this.state.address,
      events_joined: 0,
      events_made: 0,
      reputation: 0,
      player_id: ""
    }
    try {
      await AuthController.postUserDetails(user)
      this.props.navigation.navigate("Tab")
    } catch (err) {
      alert(err.message)
    }
  }

  handleFullNameInputChange(value: string): void {
    this.setState({ fullName: value })
  }

  handlePhoneNumberInputChange(value: string): void {
    this.setState({ phoneNumber: value })
  }

  handleGenderInputChange(value: string): void {
    this.setState({ gender: value })
  }

  handleFavSportInputChange(value: string): void {
    this.setState({ favSport: value })
  }

  handleAddressInputChange(value: string): void {
    this.setState({ address: value })
  }

  render() {
    return (
      <KeyboardAwareScrollView
        style={styles.container}
        contentContainerStyle={{ alignItems: "center" }}
      >
        <CustomTextInput
          placeholder={"Nama Lengkap"}
          autoCapitalize={"words"}
          onChangeText={this.handleFullNameInputChange}
        />
        <CustomTextInput
          placeholder={"Alamat"}
          onChangeText={this.handleAddressInputChange}
        />
        <CustomTextInput
          placeholder={"Nomor telepon"}
          keyboardType={"phone-pad"}
          onChangeText={this.handlePhoneNumberInputChange}
        />
        <CustomTextInput
          placeholder={"Jenis Kelamin"}
          onChangeText={this.handleGenderInputChange}
        />
        <CustomTextInput
          placeholder={"Olahraga Favorit"}
          onChangeText={this.handleFavSportInputChange}
        />
        <CustomButton label={"Lanjutkan"} onPress={this.handleSubmitButtonPressed} />
      </KeyboardAwareScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20
  }
})
