import firebase from "react-native-firebase"
import { User } from "./AuthController"
import { DataSnapshot } from "react-native-firebase/database"
import { AsyncStorage, DeviceEventEmitter } from "react-native"

class UserController {
  static getUserData = async (): Promise<User> => {
    let uid = firebase.auth().currentUser!.uid
    let userDataSnapshot: DataSnapshot = await firebase
      .database()
      .ref(`/users/${uid}`)
      .once("value")
    let userData: User = userDataSnapshot.val()
    return userData
  }

  static getUserEmail = async (): Promise<string | null> => {
    let email = await AsyncStorage.getItem("sessionemail")
    return email
  }

  static getUserId = (): string => {
    let uid = firebase.auth().currentUser!.uid
    return uid
  }

  static getUserById = async (uid: string): Promise<User> => {
    let userDataSnapshot: DataSnapshot = await firebase
      .database()
      .ref(`/users/${uid}`)
      .once("value")
    let userData: User = userDataSnapshot.val()
    return userData
  }

  static editProfile = async (user: User): Promise<boolean> => {
    let uid = firebase.auth().currentUser!.uid
    let userRef = firebase.database().ref(`/users/${uid}`)
    await userRef.set(user)
    DeviceEventEmitter.emit("shouldRefreshData")
    return true
  }

  static setPlayerId = async (id: string): Promise<boolean> => {
    let uid = firebase.auth().currentUser!.uid
    let userRef = firebase.database().ref(`/users/${uid}`)
    await userRef.child("player_id").set(id)
    console.log("success")
    return true
  }
}

export default UserController
