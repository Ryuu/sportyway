import React from "react"
import { View, Text, StyleSheet, FlatList } from "react-native"
import { NavigationStackScreenOptions, NavigationScreenProp } from "react-navigation"
import UserItem from "../components/UserItem"
import { User } from "../controller/AuthController"
import { Event } from "../controller/EventController"
import UserController from "../controller/UserController"

interface Props {
  navigation: NavigationScreenProp<any, any>
}

interface State {
  members: Array<User>
  isDataLoading: boolean
}

export default class EventJoined extends React.Component<Props, State> {
  static navigationOptions: NavigationStackScreenOptions = {
    title: "Yang Telah Bergabung"
  }

  state = {
    members: [],
    isDataLoading: false
  }

  constructor(props: Props) {
    super(props)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  async componentDidMount() {
    this.setState({ isDataLoading: true })
    const event: Event = this.props.navigation.getParam("event")
    let members: Array<User> = []
    for (let index in event.members) {
      let user = await UserController.getUserById(event.members[index])
      members.push(user)
    }
    this.setState({ members, isDataLoading: false })
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.members}
          renderItem={({ item }) => <UserItem user={item} />}
          onRefresh={this.componentDidMount}
          refreshing={this.state.isDataLoading}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    alignItems: "center"
  }
})
