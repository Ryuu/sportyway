import React from "react"
import {
  View,
  Image,
  StyleSheet,
  StatusBar,
  Text,
  AsyncStorage,
  Alert,
  Keyboard,
  ActivityIndicator,
  ImageStyle
} from "react-native"
import {
  NavigationStackScreenOptions,
  NavigationScreenProp,
  StackActions,
  NavigationActions
} from "react-navigation"
import KeyboardSpacer from "react-native-keyboard-spacer"

import CustomTextInput from "../components/CustomTextInput"
import CustomButton from "../components/CustomButton"
import metrics from "../config/metrics"
import AuthController from "../controller/AuthController"

const LOGO = require("../../assets/logo.png")
const ICON_EMAIL = require("../../assets/ic_email.png")
const ICON_KEY = require("../../assets/ic_key.png")

interface Props {
  navigation: NavigationScreenProp<any, any>
}

interface State {
  email: string
  password: string
  isEmailValid: boolean
  isLoggingIn: boolean
}

export default class Login extends React.Component<Props, State> {
  static navigationOptions: NavigationStackScreenOptions = {
    title: "Masuk"
  }

  constructor(props: Props) {
    super(props)
    this.handleRegisterButtonPressed = this.handleRegisterButtonPressed.bind(this)
    this.handleLoginButtonPressed = this.handleLoginButtonPressed.bind(this)
    this.validateEmailAddress = this.validateEmailAddress.bind(this)
    this.handleEmailInputChange = this.handleEmailInputChange.bind(this)
    this.handlePasswordInputChage = this.handlePasswordInputChage.bind(this)
  }

  // Initial state
  state = {
    email: "",
    password: "",
    isEmailValid: true,
    isLoggingIn: false
  }

  handleRegisterButtonPressed(): void {
    this.props.navigation.navigate("Register")
  }

  async handleLoginButtonPressed(): Promise<void> {
    Keyboard.dismiss()
    this.setState({ isLoggingIn: true })
    // Set the session in device's storage
    try {
      await AuthController.login(this.state.email, this.state.password)
      // Make a reset action to clear the stack screens and navigate to the desired screen
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "Tab" })]
      })
      // Call the reset action
      this.props.navigation.dispatch(resetAction)
    } catch (err) {
      Alert.alert("Error", "Email atau password yang anda masukkan salah")
    } finally {
      this.setState({ isLoggingIn: false })
    }
  }

  validateEmailAddress(): void {
    const email = this.state.email
    // Regex for email validation
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    // Return the result in boolean
    this.setState({ isEmailValid: re.test(String(email).toLowerCase()) })
  }

  handleEmailInputChange(value: string): void {
    this.setState({ email: value })
  }

  handlePasswordInputChage(value: string): void {
    this.setState({ password: value })
  }

  render() {
    // Bug confirmed from TS team about style error below but everything works fine nonetheless
    // I know it's annoying but ¯\_(ツ)_/¯
    return (
      <View style={styles.container}>
        <StatusBar barStyle={"light-content"} />
        <Image source={LOGO} style={styles.logo as ImageStyle} resizeMode={"contain"} />
        <CustomTextInput
          placeholder={"Email"}
          icon={ICON_EMAIL}
          onBlur={this.validateEmailAddress}
          onChangeText={this.handleEmailInputChange}
          keyboardType={"email-address"}
        />
        {!this.state.isEmailValid ? (
          <Text style={styles.emailPrompt}>Please input a valid email</Text>
        ) : null}
        <CustomTextInput
          placeholder={"Password"}
          secureTextEntry
          icon={ICON_KEY}
          onChangeText={this.handlePasswordInputChage}
        />
        {this.state.isLoggingIn ? (
          <ActivityIndicator />
        ) : (
          <CustomButton label={"Masuk"} onPress={this.handleLoginButtonPressed} />
        )}
        <KeyboardSpacer />
        <View style={styles.registerPrompt}>
          <Text>Belum punya akun? </Text>
          <Text style={styles.registerButton} onPress={this.handleRegisterButtonPressed}>
            Daftar disini
          </Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 50
  },

  logo: {
    width: metrics.DEVICE_WIDTH * 0.8,
    marginBottom: 20
  },

  registerPrompt: {
    position: "absolute",
    bottom: 70,
    flexDirection: "row",
    alignSelf: "center"
  },

  registerButton: {
    color: metrics.PRIMARY_COLOR
  },

  emailPrompt: {
    color: "red"
  }
})
