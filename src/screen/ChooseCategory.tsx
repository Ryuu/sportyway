import React from "react"
import { View, Text, StyleSheet, FlatList } from "react-native"
import { NavigationStackScreenOptions, NavigationScreenProp } from "react-navigation"
import CategoryItem from "../components/CategoryItem"

const CATEGORY_JOGGING = require("../../assets/sport_category_jogging.png")
const CATEGORY_FOOTBALL = require("../../assets/sport_category_football.png")
const CATEGORY_BADMINTON = require("../../assets/sport_category_badminton.png")
const CATEGORY_SWIM = require("../../assets/sport_category_swim.png")
const CATEGORY_BASKET = require("../../assets/sport_category_basket.png")

interface Props {
  navigation: NavigationScreenProp<any, any>
}

export default class ChooseCategory extends React.Component<Props, any> {
  static navigationOptions: NavigationStackScreenOptions = {
    title: "Pilih Kategori"
  }

  constructor(props: Props) {
    super(props)
  }

  handleCategoryItemPressed(category: string) {
    this.props.navigation.navigate("CreateEvent", { category: category })
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={items}
          renderItem={({ item }) => (
            <CategoryItem
              image={item.image}
              name={item.name}
              onPress={this.handleCategoryItemPressed.bind(this, item.name)}
            />
          )}
          numColumns={2}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
})

const items = [
  {
    key: "1",
    name: "Jogging",
    image: CATEGORY_JOGGING
  },
  {
    key: "2",
    name: "Futsal",
    image: CATEGORY_FOOTBALL
  },
  {
    key: "3",
    name: "Bulutangkis",
    image: CATEGORY_BADMINTON
  },
  {
    key: "4",
    name: "Renang",
    image: CATEGORY_SWIM
  },
  {
    key: "5",
    name: "Basket",
    image: CATEGORY_BASKET
  }
]
