import React from "react"
import { View, StyleSheet, TextInputProps, ImageSourcePropType, Image, TextInput } from "react-native"
import metrics from "../config/metrics"

interface Props extends TextInputProps {
  icon?: ImageSourcePropType
}

export default class CustomTextInput extends React.Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        {this.props.icon ? <Image source={this.props.icon} resizeMode={"contain"} style={styles.iconStyle} /> : null}
        <TextInput {...this.props} style={styles.textInput} underlineColorAndroid={"transparent"} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: metrics.DEVICE_WIDTH * 0.8,
    height: 50,
    backgroundColor: "white",
    borderRadius: 3,
    borderWidth: 1,
    borderColor: metrics.BORDER_COLOR,
    justifyContent: "center",
    flexDirection: "row",
    marginVertical: 5,
    padding: 5
  },

  iconStyle: {
    height: 10,
    width: 10,
    flex: 1,
    alignSelf: "center"
  },

  textInput: {
    flex: 7
  }
})
