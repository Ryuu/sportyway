import { Dimensions, StyleSheet, Platform } from "react-native"
import { DataSnapshot } from "react-native-firebase/database"

const { width, height } = Dimensions.get("window")

const styles = StyleSheet.create({
  tabBarIcon: {
    height: 20,
    width: 20
  }
})

// List of month names to convert
const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
]

const getMonthName = (month: number): string => {
  return monthNames[month]
}

function snapshotToArray<T>(snapshot: DataSnapshot): Array<T> {
  let snapshotVal = snapshot.val()
  let dataArr: Array<T> = []
  for (let key in snapshotVal) {
    snapshotVal[key].key = key
    dataArr.push(snapshotVal[key])
  }
  return dataArr
}

export default {
  PRIMARY_COLOR: "rgb(238, 133, 44)",
  BORDER_COLOR: "#EFEFEF",
  SHADOW_COLOR: "rgba(184, 184, 184, 0.5)",
  DANGER_COLOR: "rgb(218,55,73)",
  DEVICE_WIDTH: width,
  DEVICE_HEIGHT: height,
  TAB_BAR_ICON_STYLE: styles.tabBarIcon,
  OS: Platform.OS,
  IS_IPHONE_X: Platform.OS === "ios" && (height === 812 || width === 812),
  GET_MONTH_NAME: getMonthName,
  snapshotToArray: snapshotToArray
}
