import React from "react"
import {
  View,
  StyleSheet,
  Image,
  Keyboard,
  Alert,
  ActivityIndicator,
  ImageStyle
} from "react-native"
import { NavigationStackScreenOptions, NavigationScreenProp } from "react-navigation"
import metrics from "../config/metrics"
import CustomTextInput from "../components/CustomTextInput"
import CustomButton from "../components/CustomButton"
import AuthController from "../controller/AuthController"
import KeyboardSpacer from "react-native-keyboard-spacer"
import UserController from "../controller/UserController"

const LOGO = require("../../assets/logo.png")
const ICON_EMAIL = require("../../assets/ic_email.png")
const ICON_KEY = require("../../assets/ic_key.png")

interface Props {
  navigation: NavigationScreenProp<any, any>
}

interface State {
  isRegistering: boolean
  email: string
  password: string
  confirmPassword: string
  isEmailValid: boolean
}

export default class Register extends React.Component<Props, State> {
  static navigationOptions: NavigationStackScreenOptions = {
    title: "Daftar"
  }

  constructor(props: Props) {
    super(props)
    this.handleEmailInputChange = this.handleEmailInputChange.bind(this)
    this.handlePasswordInputChange = this.handlePasswordInputChange.bind(this)
    this.handleConfirmPasswordInputChange = this.handleConfirmPasswordInputChange.bind(
      this
    )
    this.validateEmailAddress = this.validateEmailAddress.bind(this)
    this.handleRegisterButtonPressed = this.handleRegisterButtonPressed.bind(this)
  }

  state = {
    isRegistering: false,
    email: "",
    password: "",
    confirmPassword: "",
    isEmailValid: true
  }

  validateEmailAddress(): void {
    const email = this.state.email
    // Regex for email validation
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    // Return the result in boolean
    this.setState({ isEmailValid: re.test(String(email).toLowerCase()) })
  }

  handleEmailInputChange(value: string): void {
    this.setState({ email: value })
  }

  handlePasswordInputChange(value: string): void {
    this.setState({ password: value })
  }

  handleConfirmPasswordInputChange(value: string): void {
    this.setState({ confirmPassword: value })
  }

  async handleRegisterButtonPressed(): Promise<void> {
    Keyboard.dismiss()
    if (this.state.password === this.state.confirmPassword) {
      this.setState({ isRegistering: true })
      try {
        let registered = await AuthController.register(
          this.state.email,
          this.state.password
        )
        if (registered) {
          AuthController.setUserSessionLoggedIn(
            this.state.email,
            this.state.password,
            UserController.getUserId()
          )
          Alert.alert("Sukses", "Anda telah terdaftar", [
            {
              text: "Lanjutkan",
              onPress: () => this.props.navigation.navigate("InputUserDetail")
            }
          ])
        }
      } catch (err) {
        console.log(err.code)
        let message
        switch (err.code) {
          case "auth/invalid-email":
            message = "Format email yang dimasukkan salah"
            break
          case "auth/email-already-in-use":
            message = "Email yang dimasukkan sudah terdaftar sebelumnya"
            break
          case "auth/weak-password":
            message = "Password minimal 6 karakter"
            break
          default:
            message = err.message
        }
        Alert.alert("Error", message)
      } finally {
        this.setState({ isRegistering: false })
      }
    } else {
      Alert.alert("Error", "Password doesn't match")
    }
  }

  render() {
    // Bug confirmed from TS team about style error below but everything works fine nonetheless
    // I know it's annoying but ¯\_(ツ)_/¯
    return (
      <View style={styles.container}>
        <Image source={LOGO} style={styles.logo as ImageStyle} resizeMode={"contain"} />
        <CustomTextInput
          placeholder={"Email"}
          icon={ICON_EMAIL}
          onChangeText={this.handleEmailInputChange}
          keyboardType={"email-address"}
          onBlur={this.validateEmailAddress}
        />
        <CustomTextInput
          placeholder={"Password"}
          secureTextEntry
          icon={ICON_KEY}
          onChangeText={this.handlePasswordInputChange}
        />
        <CustomTextInput
          placeholder={"Konfirmasi Password"}
          secureTextEntry
          icon={ICON_KEY}
          onChangeText={this.handleConfirmPasswordInputChange}
        />
        {this.state.isRegistering ? (
          <ActivityIndicator />
        ) : (
          <CustomButton label={"Register"} onPress={this.handleRegisterButtonPressed} />
        )}
        <KeyboardSpacer />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  logo: {
    width: metrics.DEVICE_WIDTH * 0.8,
    marginBottom: 20
  }
})
