import React from "react"
import { View, Image, StyleSheet } from "react-native"

const MARKER_BASKET = require("../../assets/marker_basket.png")
const MARKER_BADMINTON = require("../../assets/marker_badminton.png")
const MARKER_FOOTBALL = require("../../assets/marker_football.png")
const MARKER_JOGGING = require("../../assets/marker_jogging.png")
const MARKER_SWIM = require("../../assets/marker_swim.png")

interface Props {
  type: string
}

export default class MapMarker extends React.Component<Props, any> {
  render() {
    switch (this.props.type) {
      case "Basket":
        return (
          <View style={styles.container}>
            <Image source={MARKER_BASKET} style={styles.image} resizeMode={"contain"} />
          </View>
        )
      case "Badminton":
        return (
          <View style={styles.container}>
            <Image
              source={MARKER_BADMINTON}
              style={styles.image}
              resizeMode={"contain"}
            />
          </View>
        )
      case "Jogging":
        return (
          <View style={styles.container}>
            <Image source={MARKER_JOGGING} style={styles.image} resizeMode={"contain"} />
          </View>
        )
      case "Swim":
        return (
          <View style={styles.container}>
            <Image source={MARKER_SWIM} style={styles.image} resizeMode={"contain"} />
          </View>
        )
      case "Football":
        return (
          <View style={styles.container}>
            <Image source={MARKER_FOOTBALL} style={styles.image} resizeMode={"contain"} />
          </View>
        )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    paddingBottom: 10
  },

  image: {
    width: 70,
    height: 70
  }
})
