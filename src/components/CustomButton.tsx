import React from "react"
import { TouchableOpacity, Text, StyleSheet, ViewStyle } from "react-native"
import metrics from "../config/metrics"

const styles = StyleSheet.create({
  button: {
    width: metrics.DEVICE_WIDTH * 0.8,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    shadowColor: metrics.SHADOW_COLOR,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 5,
    marginVertical: 5
  },

  label: {
    color: "white"
  }
})

type Props = {
  label: String
  style?: ViewStyle
} & Partial<DefaultProps>

type DefaultProps = Readonly<typeof defaultProps>

const defaultProps = {
  backgroundColor: metrics.PRIMARY_COLOR as String,
  onPress: () => console.log("Button pressed") as void,
  style: styles.button as ViewStyle
}

export default class CustomButton extends React.Component<Props> {
  static defaultProps = defaultProps

  render() {
    const backgroundColor = this.props.backgroundColor!
    const style = this.props.style!
    return (
      <TouchableOpacity onPress={this.props.onPress} style={Object.assign({ backgroundColor: backgroundColor }, style)}>
        <Text style={styles.label}>{this.props.label}</Text>
      </TouchableOpacity>
    )
  }
}
