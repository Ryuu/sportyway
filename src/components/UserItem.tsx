import React from "react"
import { View, Text, Image, StyleSheet } from "react-native"
import metrics from "../config/metrics"
import { User } from "../controller/AuthController"

const DEFAULT_AVA = require("../../assets/ava_example.png")

interface Props {
  user: User
}

export default class UserItem extends React.Component<Props, any> {
  render() {
    const user = this.props.user
    return (
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <Image source={DEFAULT_AVA} style={styles.image} />
        </View>
        <View style={styles.detailContainer}>
          <Text>{user.full_name}</Text>
          <Text>{user.phone_number}</Text>
          <Text>{user.gender}</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 5,
    borderRadius: 5,
    width: metrics.DEVICE_WIDTH * 0.9,
    height: 100,
    shadowColor: metrics.SHADOW_COLOR,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 5,
    padding: 10,
    backgroundColor: "white",
    flexDirection: "row"
  },

  imageContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  image: {
    width: 70,
    height: 70,
    borderRadius: 35
  },

  detailContainer: {
    flex: 3,
    padding: 20,
    justifyContent: "space-between"
  }
})
