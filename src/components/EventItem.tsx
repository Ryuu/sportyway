import React from "react"
import { StyleSheet, View, Image, Text, TouchableOpacity, ImageStyle } from "react-native"
import metrics from "../config/metrics"

const ICON_GENDER = require("../../assets/ic_gender_male.png")
const ICON_TIME = require("../../assets/ic_time.png")
const ICON_LOCATION = require("../../assets/ic_location.png")
const ICON_MONEY = require("../../assets/ic_money.png")

const CATEGORY_JOGGING = require("../../assets/sport_category_jogging.png")
const CATEGORY_FOOTBALL = require("../../assets/sport_category_football.png")
const CATEGORY_BADMINTON = require("../../assets/sport_category_badminton.png")
const CATEGORY_SWIM = require("../../assets/sport_category_swim.png")
const CATEGORY_BASKET = require("../../assets/sport_category_basket.png")

interface Props {
  onPress?: () => void
  title: string
  time: string
  quota: number
  location: string
  price: number
  type: string
  date: string
}

export default class EventItem extends React.Component<Props, any> {
  // Get the corresponding category image based on event type
  getCategoryImage() {
    switch (this.props.type) {
      case "Jogging":
        return CATEGORY_JOGGING
      case "Football":
        return CATEGORY_FOOTBALL
      case "Badminton":
        return CATEGORY_BADMINTON
      case "Swim":
        return CATEGORY_SWIM
      case "Basket":
        return CATEGORY_BASKET
    }
  }

  render() {
    const { title, time, quota, location, price, date } = this.props
    return (
      <TouchableOpacity style={styles.container} onPress={this.props.onPress}>
        <Text style={styles.title}>{title}</Text>
        <Image
          source={this.getCategoryImage()}
          resizeMode={"contain"}
          style={styles.image as ImageStyle}
        />
        <View style={styles.contentContainer}>
          <View style={styles.contentItemContainer}>
            <Image
              source={ICON_GENDER}
              style={styles.icon as ImageStyle}
              resizeMode={"contain"}
            />
            <Text>{quota} Orang</Text>
          </View>
          <View style={styles.contentItemContainer}>
            <Image
              source={ICON_TIME}
              style={styles.icon as ImageStyle}
              resizeMode={"contain"}
            />
            <Text>
              {date} {time}
            </Text>
          </View>
          <View style={styles.contentItemContainer}>
            <Image
              source={ICON_LOCATION}
              style={styles.icon as ImageStyle}
              resizeMode={"contain"}
            />
            <Text>{location}</Text>
          </View>
          <View style={styles.contentItemContainer}>
            <Image
              source={ICON_MONEY}
              style={styles.icon as ImageStyle}
              resizeMode={"contain"}
            />
            <Text>Rp. {price}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: metrics.DEVICE_WIDTH * 0.9,
    height: 150,
    shadowColor: metrics.SHADOW_COLOR,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 5,
    flexDirection: "row",
    padding: 10,
    backgroundColor: "white",
    borderRadius: 5,
    marginVertical: 5
  },

  image: {
    flex: 1,
    alignSelf: "center",
    marginRight: 10
  },

  contentContainer: {
    flex: 2,
    marginLeft: 5,
    justifyContent: "center"
  },

  title: {
    position: "absolute",
    top: 10,
    left: 10
  },

  time: {
    position: "absolute",
    top: 10,
    right: 10
  },

  contentItemContainer: {
    flexDirection: "row",
    marginVertical: 5
  },

  icon: {
    width: 15,
    height: 15,
    marginRight: 10,
    alignSelf: "center"
  }
})
