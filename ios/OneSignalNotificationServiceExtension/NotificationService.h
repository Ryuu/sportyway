//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Ananta Pratama on 27/11/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
